
Sample request

http://localhost:8090/quote/search/v1
HTTP Method : POST
{
                "postCode": "2000",
                "occupation": "Plumber",
                "turnover": 500000
            }
The Port and other config properties are under /config folder.

Implementation details.


The code is using the Drool rule engine . The rules are available in the resources folder.

The data is kept in the /config/insurerdb.json file, this will be loaded to memory when the application loads.

//TODO 
The dynamic insertion of exclusion list to drool rules didn't work, needs more reading about drool :( 
This led to an ugly way of hard coding the exclusion rules  in rule file for each insurer.