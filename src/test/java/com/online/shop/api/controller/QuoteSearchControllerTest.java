package com.online.shop.api.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quote.search.api.application.QuoteSearchpApiApplication;
import com.quote.search.api.model.ApiResponse;
import com.quote.search.api.model.PolicyQuoteSearchResult;
import com.quote.search.api.model.PolicySearch;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = QuoteSearchpApiApplication.class)
@AutoConfigureMockMvc
public class QuoteSearchControllerTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void testQuoteSearchForOneQuotes() throws Exception {

		MvcResult mvcResult = mvc
				.perform(
						post("/quote/search/v1").content(objectToJsonString(new PolicySearch("2000", "Butcher", 8000L)))
								.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andReturn();
		ApiResponse<PolicyQuoteSearchResult> test = jsconStringToObject(mvcResult.getResponse().getContentAsString());
		assertEquals(test.getData().getQuoteVos().size(), 1);
		assertTrue(test.getIsSuccess());
		assertTrue(test.getErrorMessages().isEmpty());
		test.getData().getQuoteVos().forEach(quote -> {
			assertFalse(Arrays.asList("Insurer2", "Insurer3", "Insurer4", "Insurer5").contains(quote.getName()));

		});
	}

	@Test
	public void testQuoteSearchForThreeQuotes() throws Exception {

		MvcResult mvcResult = mvc
				.perform(post("/quote/search/v1")
						.content(objectToJsonString(new PolicySearch("2000", "Plumber", 500000L)))
						.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andReturn();
		ApiResponse<PolicyQuoteSearchResult> test = jsconStringToObject(mvcResult.getResponse().getContentAsString());
		assertEquals(test.getData().getQuoteVos().size(), 3);
		assertTrue(test.getIsSuccess());
		assertTrue(test.getErrorMessages().isEmpty());
		test.getData().getQuoteVos().forEach(quote -> {
			assertFalse(Arrays.asList("Insurer2", "Insurer3").contains(quote.getName()));

		});
	}

	/**
	 * Converts from Object to Json String
	 * 
	 * @param obj
	 * @return
	 */
	private String objectToJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			final String jsonContent = mapper.writeValueAsString(obj);
			return jsonContent;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Converts Json string to <code>ApiResponse<PolicyQuoteSearchResult></code>.
	 * 
	 * @param jsonString - json String
	 * @return - ApiResponse<PolicyQuoteSearchResult>
	 */
	private ApiResponse<PolicyQuoteSearchResult> jsconStringToObject(final String jsonString) {
		try {
			ApiResponse<PolicyQuoteSearchResult> test = new ApiResponse<PolicyQuoteSearchResult>();

			final ObjectMapper mapper = new ObjectMapper();
			test = mapper.readValue(jsonString,
					mapper.getTypeFactory().constructParametricType(ApiResponse.class, PolicyQuoteSearchResult.class));

			return test;
		} catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}

}
