package com.quote.search.api.exception;

public class DataNotFoudException extends RuntimeException{

	public DataNotFoudException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DataNotFoudException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public DataNotFoudException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public DataNotFoudException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public DataNotFoudException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
