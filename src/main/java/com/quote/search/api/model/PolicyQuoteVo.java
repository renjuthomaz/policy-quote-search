package com.quote.search.api.model;

/**
 * Model to send the quote response.
 * 
 * @author renju.thomas
 *
 */
public class PolicyQuoteVo {
	private String name;
	private Long amount;

	public PolicyQuoteVo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PolicyQuoteVo(String name, Long amount) {
		super();
		this.name = name;
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

}
