package com.quote.search.api.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to represent the API reponse.
 * 
 * @author renju.thomas
 *
 * @param <T>
 */
public class ApiResponse<T> {

	private Boolean isSuccess;
	private T data;
	List<ErrorMessage> errorMessages = new ArrayList<ErrorMessage>(1);

	public Boolean getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public List<ErrorMessage> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<ErrorMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
