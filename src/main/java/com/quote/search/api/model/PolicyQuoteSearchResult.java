package com.quote.search.api.model;

import java.util.ArrayList;
import java.util.List;

import com.quote.search.api.domain.Insurer;

/**
 * The class to represent the list of quote results.
 * 
 * @author renju.thomas
 *
 */
public class PolicyQuoteSearchResult {

	private List<PolicyQuoteVo> quoteVos;

	public PolicyQuoteSearchResult() {
		super();
	}

	public PolicyQuoteSearchResult(List<Insurer> quotes) {
		super();

		this.quoteVos = new ArrayList<PolicyQuoteVo>();
		quotes.forEach(quote -> {
			this.quoteVos.add(new PolicyQuoteVo(quote.getName(), quote.getAmount()));
		});
	}

	public List<PolicyQuoteVo> getQuoteVos() {
		return quoteVos;
	}

	public void setQuoteVos(List<PolicyQuoteVo> quoteVos) {
		this.quoteVos = quoteVos;
	}

}
