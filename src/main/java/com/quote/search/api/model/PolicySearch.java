package com.quote.search.api.model;

import java.io.Serializable;

/**
 * The encapsulation to represent the search request.
 * 
 * @author renju.thomas
 *
 */
public class PolicySearch implements Serializable {

	private String postCode;
	private String occupation;
	private Long turnover;
	private String insurerId;

	public PolicySearch() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PolicySearch(String postCode, String occupation, Long turnover) {
		super();
		this.postCode = postCode;
		this.occupation = occupation;
		this.turnover = turnover;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public Long getTurnover() {
		return turnover;
	}

	public void setTurnover(Long turnover) {
		this.turnover = turnover;
	}

	public String getInsurerId() {
		return insurerId;
	}

	public void setInsurerId(String insurerId) {
		this.insurerId = insurerId;
	}
}
