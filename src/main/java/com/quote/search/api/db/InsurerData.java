package com.quote.search.api.db;

import java.util.List;

import com.quote.search.api.domain.Insurer;

/**
 * Interface to access the insurance db data structure.
 * 
 * @author renju.thomas
 *
 */
public interface InsurerData {

	/**
	 * Method to get the list of Insurers from the list of insurer Ids.
	 * 
	 * @param insurerIds
	 *            - List od ids.
	 * @return - List of <code>Insurer</code>.
	 */
	List<Insurer> getInsurersByInsurerIds(List<String> insurerIds);
}
