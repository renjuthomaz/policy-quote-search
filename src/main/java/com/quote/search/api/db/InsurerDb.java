package com.quote.search.api.db;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.quote.search.api.domain.InsurerDataStructure;
import com.quote.search.api.domain.Insurer;
import com.quote.search.api.exception.ApiGeneralException;

/**
 * Mock data source to hold the quote details.
 * 
 * @author renju.thomas
 *
 */
@Component
public class InsurerDb {

	@Value("${policy.search.insurer.db}")
	private String dbJsonLocation;

	private InsurerDataStructure insurerDs;

	/**
	 * Initializes the mock daa structure.
	 */
	@PostConstruct
	public void initializeInsurerDB() {
		try {
			ObjectMapper mapper = new ObjectMapper();
			this.insurerDs = mapper.readValue(new File(dbJsonLocation), InsurerDataStructure.class);
		} catch (Exception e) {
			throw new ApiGeneralException("Unable to initialize the Insurers data base.", e);
		}
	}

	/**
	 * Get all insurers in the DB.
	 * 
	 * @return
	 */
	public Set<Insurer> getAllInsurers() {
		Set<Insurer> insurers = new HashSet<Insurer>();
		insurerDs.getInsurers().forEach((insurerId, insurer) -> {
			insurers.add(insurer);
		});
		return insurers;
	}

	/**
	 * Get the list of matching quotes.
	 * 
	 * @param insurerIds
	 * @return - The list of matching <code> Quote</code>.
	 */
	public List<Insurer> getQuotesByInsurerIds(List<String> insurerIds) {
		List<Insurer> matchingQuotes = new ArrayList<Insurer>();

		insurerIds.forEach(insurerId -> {
			if (insurerDs.getInsurers().containsKey(insurerId)) {
				matchingQuotes.add((Insurer) this.insurerDs.getInsurers().get(insurerId));
			}
		});
		return matchingQuotes;
	}

}
