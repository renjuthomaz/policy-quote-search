package com.quote.search.api.data.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.quote.search.api.db.InsurerData;
import com.quote.search.api.db.InsurerDb;
import com.quote.search.api.domain.Insurer;

/**
 * Implementation to access the insurance db data structure.
 * 
 * @author renju.thomas
 *
 */
@Component
public class InsurerDataImpl implements InsurerData {
	
	@Autowired
	InsurerDb insurerDb;

	/* (non-Javadoc)
	 * @see com.quote.search.api.db.InsurerData#getInsurersByInsurerIds(java.util.List)
	 */
	public List<Insurer> getInsurersByInsurerIds(List<String> insurerIds) {
		return insurerDb.getQuotesByInsurerIds(insurerIds);
	}
}
