package com.quote.search.api.util;

import java.util.List;

import com.quote.search.api.domain.Insurer;
import com.quote.search.api.model.ApiResponse;
import com.quote.search.api.model.PolicyQuoteSearchResult;

/**
 * @author renju.thomas
 *
 */
public class QuoteResultTransformer {
	/**
	 * Converting the List of <code> Insurer </code> to <code> List<Insurer> </code>
	 * 
	 * @param quotes
	 * @return
	 */
	public static ApiResponse<PolicyQuoteSearchResult> getApiResponseFromQuoteList(List<Insurer> quotes) {
		ApiResponse<PolicyQuoteSearchResult> apiResponse = new ApiResponse<PolicyQuoteSearchResult>();
		apiResponse.setData(new PolicyQuoteSearchResult(quotes));
		apiResponse.setIsSuccess(Boolean.TRUE);
		return apiResponse;
	}
}
