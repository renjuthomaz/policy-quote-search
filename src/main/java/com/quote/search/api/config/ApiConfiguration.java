package com.quote.search.api.config;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.runtime.KieContainer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Api Configurations
 * 
 * @author renju.thomas
 *
 */

@Configuration
@PropertySource("file:./config/application.properties")
public class ApiConfiguration {

	@Value("${policy.search.rule.location}")
	private String policyRules;
	
	/**
	 * The KieContainer for the Drule rules.
	 * 
	 * @return
	 */
	@Bean
    public KieContainer kieContainer() {
        KieServices kieServices = KieServices.Factory.get();
 
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        kieFileSystem.write(org.kie.internal.io.ResourceFactory.newClassPathResource(policyRules));
        KieBuilder kieBuilder = kieServices.newKieBuilder(kieFileSystem);
        kieBuilder.buildAll();
        KieModule kieModule = kieBuilder.getKieModule();
 
        return kieServices.newKieContainer(kieModule.getReleaseId());
    }

}
