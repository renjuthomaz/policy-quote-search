package com.quote.search.api.service;

import java.util.List;

import com.quote.search.api.domain.Insurer;
import com.quote.search.api.model.PolicySearch;

/**
 * @author renju.thomas
 *
 */
public interface InsurerSearchService {

	/**
	 * Method to do the search for a criteria.
	 * 
	 * @param policySearch
	 * @return
	 */
	List<Insurer> searchInsurer(final PolicySearch policySearch);
}
