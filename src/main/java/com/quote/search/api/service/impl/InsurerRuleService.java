package com.quote.search.api.service.impl;

import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quote.search.api.domain.PolicySearchRuleResult;
import com.quote.search.api.model.PolicySearch;

@Service
public class InsurerRuleService {

	@Autowired
	private KieContainer kieContainer;

	// TODO All attempts to pass the exclusion lists to the Drool rules and also the
	// attempts to keep only one rule failed. Read more about drool :(
	/**
	 * Method to execute the rule engine and get the list of matching insurer ids.
	 * 
	 * @param policySearch
	 * @return
	 */
	public PolicySearchRuleResult getMatchinInsurers(final PolicySearch policySearch) {

		PolicySearchRuleResult ruleResult = new PolicySearchRuleResult();
		KieSession kieSession = kieContainer.newKieSession();
		kieSession.setGlobal("ruleResult", ruleResult);
		kieSession.insert(policySearch);
		kieSession.fireAllRules();
		kieSession.dispose();
		return ruleResult;
	}

}