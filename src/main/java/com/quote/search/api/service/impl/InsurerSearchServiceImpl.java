package com.quote.search.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quote.search.api.db.InsurerData;
import com.quote.search.api.domain.PolicySearchRuleResult;
import com.quote.search.api.domain.Insurer;
import com.quote.search.api.model.PolicySearch;
import com.quote.search.api.service.InsurerSearchService;

/**
 * @author renju.thomas
 *
 */
@Service
public class InsurerSearchServiceImpl implements InsurerSearchService {

	@Autowired
	InsurerData customerData;

	@Autowired
	InsurerRuleService insuranceRuleService;

	/* (non-Javadoc)
	 * @see com.quote.search.api.service.InsurerSearchService#searchInsurer(com.quote.search.api.model.PolicySearch)
	 */
	@Override
	public List<Insurer> searchInsurer(PolicySearch policySearch) {
		PolicySearchRuleResult searchRuleResult = insuranceRuleService.getMatchinInsurers(policySearch);
		List<Insurer> matchinInsureres = customerData.getInsurersByInsurerIds(searchRuleResult.getMatchingResult());
		return matchinInsureres;
	}

}
