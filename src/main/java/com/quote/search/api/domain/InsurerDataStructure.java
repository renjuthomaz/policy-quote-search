package com.quote.search.api.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author renju.thomas
 *
 */
public class InsurerDataStructure implements Serializable {
	private Map<String, Insurer> insurers = new HashMap<String, Insurer>(1);

	public Map<String, Insurer> getInsurers() {
		return insurers;
	}

	public void setInsurers(Map<String, Insurer> insurers) {
		this.insurers = insurers;
	}

}
