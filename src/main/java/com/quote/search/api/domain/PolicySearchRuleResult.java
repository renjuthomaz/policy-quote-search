package com.quote.search.api.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to hold the list of matching insurer ids from the rule engine.
 * 
 * @author renju.thomas
 *
 */
public class PolicySearchRuleResult {
	private List<String> matchingResult = new ArrayList<String>();

	public List<String> getMatchingResult() {
		return matchingResult;
	}

	public void setMatchingResult(List<String> matchingResult) {
		this.matchingResult = matchingResult;
	}

}
