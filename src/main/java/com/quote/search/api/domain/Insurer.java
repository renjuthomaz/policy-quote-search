package com.quote.search.api.domain;

/**
 * Encapsulation to represent an Insurer.
 * 
 * @author renju.thomas
 *
 */
public class Insurer {
	private String insurerId;
	private String name;
	private Long amount;

	private InsurerExculsionCriteria exculsionCriteria;

	public Insurer() {
		super();
	}

	public Insurer(String insurerId, String name, Long amount) {
		super();
		this.insurerId = insurerId;
		this.name = name;
		this.amount = amount;
	}

	public String getInsurerId() {
		return insurerId;
	}

	public void setInsurerId(String insurerId) {
		this.insurerId = insurerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public InsurerExculsionCriteria getExculsionCriteria() {
		return exculsionCriteria;
	}

	public void setExculsionCriteria(InsurerExculsionCriteria exculsionCriteria) {
		this.exculsionCriteria = exculsionCriteria;
	}

}
