package com.quote.search.api.domain;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class InsurerExculsionCriteria {

	@JsonIgnore
	private Insurer insurer;
	private List<String> excludedPostCode = new ArrayList<String>(0);
	private List<String> excludedOccupations = new ArrayList<String>(0);
	private Long minimumTurnover = 0L;

	
	public InsurerExculsionCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InsurerExculsionCriteria(Insurer insurer, List<String> excludedPostCode, List<String> excludedOccupations,
			Long minimumTurnover) {
		super();
		this.insurer = insurer;
		this.excludedPostCode = excludedPostCode;
		this.excludedOccupations = excludedOccupations;
		this.minimumTurnover = minimumTurnover;
	}

	public Insurer getInsurer() {
		return insurer;
	}

	public void setInsurer(Insurer insurer) {
		this.insurer = insurer;
	}

	public List<String> getExcludedPostCode() {
		return excludedPostCode;
	}

	public void setExcludedPostCode(List<String> excludedPostCode) {
		this.excludedPostCode = excludedPostCode;
	}

	public List<String> getExcludedOccupations() {
		return excludedOccupations;
	}

	public void setExcludedOccupations(List<String> excludedOccupations) {
		this.excludedOccupations = excludedOccupations;
	}

	public Long getMinimumTurnover() {
		return minimumTurnover;
	}

	public void setMinimumTurnover(Long minimumTurnover) {
		this.minimumTurnover = minimumTurnover;
	}

}
