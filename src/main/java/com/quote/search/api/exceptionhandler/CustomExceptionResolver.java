package com.quote.search.api.exceptionhandler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.quote.search.api.exception.ApiGeneralException;
import com.quote.search.api.exception.DataNotFoudException;
import com.quote.search.api.model.ApiResponse;
import com.quote.search.api.model.ErrorMessage;

@RestControllerAdvice
public class CustomExceptionResolver extends ResponseEntityExceptionHandler {
	@ExceptionHandler(value = { ApiGeneralException.class })
	protected ResponseEntity<Object> handleGeneralExcpetion(ApiGeneralException ex, WebRequest request) {
		String bodyOfResponse = ex.getMessage();// "There is an internal server error.";
		ApiResponse<Object> apiResponse = new ApiResponse<>();
		apiResponse.setIsSuccess(Boolean.FALSE);
		ErrorMessage errorMessages = new ErrorMessage("Failed to load data", "501");

		apiResponse.getErrorMessages().add(errorMessages);
		return handleExceptionInternal(ex, apiResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

	@ExceptionHandler(value = { DataNotFoudException.class })
	protected ResponseEntity<Object> handleDataNotFound(DataNotFoudException ex, WebRequest request) {
		String bodyOfResponse = ex.getMessage();
		return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
	}

}
