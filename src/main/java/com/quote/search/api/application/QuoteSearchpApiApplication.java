package com.quote.search.api.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * API to search  quotes.
 * 
 * @author renju.thomas
 *
 */
@SpringBootApplication
@ComponentScan("com.quote.search.*")
public class QuoteSearchpApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuoteSearchpApiApplication.class, args);
	}
}
