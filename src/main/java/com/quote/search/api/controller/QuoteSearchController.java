package com.quote.search.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.quote.search.api.exception.ApiGeneralException;
import com.quote.search.api.model.ApiResponse;
import com.quote.search.api.model.PolicyQuoteSearchResult;
import com.quote.search.api.model.PolicySearch;
import com.quote.search.api.service.impl.InsurerSearchServiceImpl;
import com.quote.search.api.util.QuoteResultTransformer;

/**
 * Controller for handling the discount.
 * 
 * @author renju.thomas
 *
 */
@RestController
@RequestMapping(path = "/quote")
public class QuoteSearchController {

	@Autowired
	private InsurerSearchServiceImpl insurerServiceImpl;

	/**
	 * The controller method to search for qutes.
	 * 
	 * @param policySearch
	 *            - <code> PolicySearch </code> critria.
	 * @return List of quotes.
	 */
	@RequestMapping(path = "/search/v1", method = RequestMethod.POST)
	public @ResponseBody ApiResponse<PolicyQuoteSearchResult> searchQuotes(@RequestBody PolicySearch policySearch) {
		try {
			return QuoteResultTransformer.getApiResponseFromQuoteList(insurerServiceImpl.searchInsurer(policySearch));
		} catch (Exception exception) {
			throw new ApiGeneralException(exception);
		}
	}
}
